<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use File;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Articles::all();
        return view('articles.index', compact('article'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'article' => 'required',
            'poster' => 'mimes:jpeg,jpg,png|max:2000',
        ]);

        $poster = $request->poster;
        $new_poster = time() . ' - ' . $poster->getClientOriginalName();

        $article = Articles::create([
            'title' => $request->title,
            'article' => $request->article,
            'poster' => $new_poster,
        ]);
        $poster->move('posters/', $new_poster);

        return redirect('articles/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Articles::find($id);
        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Articles::find($id);
        return view('articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'article' => 'required',
            'poster' => 'mimes:jpeg,jpg,png|max:2000',
        ]);

        $article = Articles::findorfail($id);

        if ($request->has('poster')) {
            $path = "posters/";
            File::delete($path . $article->poster);
            $poster = $request->poster;
            $new_poster = time() . ' - ' . $poster->getClientOriginalName();
            $poster->move('posters/', $new_poster);
            $article_data = [
                'title' => $request->title,
                'article' => $request->article,
                'poster' => $new_poster,
            ];
        } else {
            $article_data = [
                'title' => $request->title,
                'article' => $request->article,
            ];
        }

        $article->update($article_data);
        return redirect('/articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Articles::findorfail($id);
        $article->delete();

        $path = "posters/";
        File::delete($path . $article->poster);

        return redirect('/articles');
    }
}
