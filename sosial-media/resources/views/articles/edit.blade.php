@extends('dashboard')

@section('content')
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <!-- Default Basic Forms Start -->
            <div class="pd-20 card-box mb-30">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="text-blue h4">Edit Your Article</h4>
                        <p class="mb-30">Make your dreams come true with writing</p>
                    </div>
                </div>
                <form action="/articles/{{$article->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" type="text" name="title" id="title" value="{{$article->title}}" placeholder="Create your own title">
                        @error('title')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="article">Textarea</label>
                        <textarea class="form-control" name="article" id="article">{{$article->article}}</textarea>
                        @error('article')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="poster">Poster</label>
                        <div class="custom-file">
                            <input type="file" name="poster" id="poster" class="custom-file-input">
                            <label class="custom-file-label">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group m-0 pt-20">
                        <button class="btn btn-primary" type="submit">Update Article</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection