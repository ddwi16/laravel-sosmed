@extends('dashboard')

@section('content')
<div class="pd-ltr-20 height-100-p xs-pd-20-10">
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="title">
                        <h4>Blog</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/home">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Blog</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="blog-wrap">
            <div class="container pd-0">
                <div class="row">
                    <div class="col-md-8 col-md-12">
                        <div class="blog-list">
                            <ul>
                                @if ($article == null)
                                    <li>
                                        <div class="row no-gutters">
                                            <div class="col-lg-8 col-md-12 col-sm-12">
                                                <div class="blog-caption">
                                                    <h4>Empty Article</h4>
                                                    <div class="blog-by">
                                                        <p>There is no article written</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @else
                                    @foreach ($article as $item)
                                    <li>
                                        <div class="row no-gutters">
                                            <div class="col-lg-4 col-md-12 col-sm-12">
                                                <div class="blog-img">
                                                    <img src="{{asset('posters/' . $item->poster)}}" alt="poster-picture" class="blog-img">
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-12 col-sm-12">
                                                <div class="blog-caption">
                                                    <h4><a href="#">{{$item->title}}</a></h4>
                                                    <div class="blog-by">
                                                        <p>{{Str::limit($item->article, 300)}}</p>
                                                        <div class="pt-5">
                                                            @auth
                                                                <a href="/articles/{{$item->id}}" class="btn btn-outline-primary">Read More</a>
                                                                <a href="/articles/{{$item->id}}/edit" class="btn btn-outline-warning">Edit</a>
                                                                <form action="/articles/{{$item->id}}" method="POST">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <input type="submit" value="delete" class="btn btn-outline-danger mt-3">
                                                                </form>
                                                            @endauth
                                                            @guest
                                                                <a href="/articles/{{$item->id}}" class="btn btn-outline-primary">Read More</a>
                                                            @endguest
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection