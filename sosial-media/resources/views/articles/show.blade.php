@extends('dashboard')

@section('content')
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="title">
                            <h4>Blog Detail</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blog Detail</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="blog-wrap">
                <div class="container pd-0">
                    <div class="row">
                        <div class="col-md-8 col-md-12">
                            <div class="blog-detail card-box overflow-hidden mb-30">
                                <div class="blog-img">
                                    <img src="{{asset('posters/' . $article->poster)}}" alt="">
                                </div>
                                <div class="blog-caption">
                                    <h4 class="mb-10">{{$article->title}}</h4>
                                    <p style="text-align: justify">{{$article->article}}</p>
                                    <a href="/articles" class="btn btn-primary">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection