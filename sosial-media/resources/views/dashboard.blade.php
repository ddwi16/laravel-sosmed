<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>DeskApp - Bootstrap Admin Dashboard HTML Template</title>

	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="{{asset('deskapp2-master/vendors/images/apple-touch-icon.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('deskapp2-master/vendors/images/favicon-32x32.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{asset('deskapp2-master/vendors/images/favicon-16x16.png')}}">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/vendors/styles/core.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/vendors/styles/icon-font.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/src/plugins/datatables/css/dataTables.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/src/plugins/datatables/css/responsive.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/vendors/styles/style.css')}}">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
</head>
<body>
	@include('partials.loader')

	@include('partials.header')

	@include('partials.sidebar_setting')

	@include('partials.sidebar_menu')
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		@yield('content')
	</div>
	<!-- js -->
	<script src="{{asset('deskapp2-master/vendors/scripts/core.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/script.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/process.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/layout-settings.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/apexcharts/apexcharts.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/responsive.bootstrap4.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/dashboard.js')}}"></script>
</body>
</html>
