<div class="left-side-bar">
    <div class="brand-logo">
        <a href="/">
            <img src="{{asset('deskapp2-master/vendors/images/deskapp-logo.svg')}}" alt="" class="dark-logo">
            <img src="{{asset('deskapp2-master/vendors/images/deskapp-logo-white.svg')}}" alt="" class="light-logo">
        </a>
        <div class="close-sidebar" data-toggle="left-sidebar-close">
            <i class="ion-close-round"></i>
        </div>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu">
            <ul id="accordion-menu">
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="micon dw dw-edit2"></span><span class="mtext">Post Article</span>
                    </a>
                    <ul class="submenu">
                        <li><a href="/articles/create">Create Article</a></li>
                        <li><a href="#">My Article</a></li>
                        <li><a href="/articles">All Article</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="micon dw dw-right-arrow1"></span><span class="mtext">User Pages</span>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="/login">Login</a>
                        </li>
                        {{-- <li><a href="{{asset('deskapp2-master/forgot-password.html')}}">Forgot Password</a></li>
                        <li><a href="{{asset('deskapp2-master/reset-password.html')}}">Reset Password</a></li> --}}
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>