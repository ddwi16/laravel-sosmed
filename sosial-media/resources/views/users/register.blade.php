@extends('home')

@section('content')
    <div class="login-header box-shadow">
        <div class="container-fluid d-flex justify-content-between align-items-center">
            <div class="brand-logo">
                <a href="/login">
                    <img src="{{asset('deskapp2-master/vendors/images/deskapp-logo.svg')}}" alt="">
                </a>
            </div>
            <div class="login-menu">
                <ul>
                    <li><a href="/login">Login</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="register-page-wrap d-flex align-items-center flex-wrap justify-content-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-lg-7">
                    <img src="{{asset('deskapp2-master/vendors/images/register-page-img.png')}}" alt="">
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="login-box bg-white box-shadow border-radius-10">
						<div class="login-title">
							<h2 class="text-center text-primary">Register To DeskApp</h2>
						</div>
						<form>
							<div class="form-wrap max-width-600 mx-auto">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Email Address*</label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Username*</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Password*</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Confirm Password*</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control">
                                    </div>
                                </div>
                            </div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group mb-0">
										<!--
											use code for form submit
											<input class="btn btn-primary btn-lg btn-block" type="submit" value="Sign In">
										-->
                                        <a class="btn btn-outline-primary btn-lg btn-block" href="#">Register To Create Account</a>
									</div>
									<div class="input-group mb-0">
									</div>
								</div>
							</div>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
    <!-- success Popup html Start -->
    <button type="button" id="success-modal-btn" hidden data-toggle="modal" data-target="#success-modal" data-backdrop="static">Launch modal</button>
    <div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered max-width-400" role="document">
            <div class="modal-content">
                <div class="modal-body text-center font-18">
                    <h3 class="mb-20">Form Submitted!</h3>
                    <div class="mb-30 text-center"><img src="{{asset('deskapp2-master/vendors/images/success.png')}}"></div>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                </div>
                <div class="modal-footer justify-content-center">
                    <a href="login.html" class="btn btn-primary">Done</a>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('deskapp2-master/vendors/scripts/core.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/script.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/process.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/layout-settings.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/jquery-steps/jquery.steps.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/steps-setting.js')}}"></script>
@endsection
